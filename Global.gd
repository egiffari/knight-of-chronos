extends Node

var level = 0
var ZA_WARUDO = false
var ZA_WARUDO_Activated = false
var playtime = 0
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
var timer = 0
var max_time = 10

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

func _process(delta: float) -> void:
	if ZA_WARUDO:
		timer += delta
		if timer > max_time:
			timer = 0
			ZA_WARUDO = false
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
