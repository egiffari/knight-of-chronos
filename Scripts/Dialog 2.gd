extends Node2D

onready var chronos = $Characters/Chronos
onready var elize = $Characters/Elize
onready var textfield = $Dialog_box/Control/Outerbox/Innerbox/textfield

var count = 0
var text_status

var dialogue = [{"Elize":"Phew.... Finally done...."},
				{"Chronos":"Good, you made it out alive."},
				{"Elize":"Barely...."},
				{"Elize":"Now.... can you explain to me what's going on??"},
				{"Chronos":"Of course..."},
				{"Chronos":"As i said, you were summoned by Gaia to that world"},
				{"Chronos":"As for why.... i suspect that she wanted to test you"},
				{"Chronos":"But it's purpose.... i'm afraid i do not know"},
				{"Chronos":"Gaia is an elusive being"},
				{"Elize":"So basically you don't know either?"},
				{"Elize":"*Sigh*"},
				{"Elize":"So what do i do now?"},
				{"Elize":"Why did you even summon me here again if you can't answer my question?"},
				{"Chronos":"To warn you."},
				{"Elize":"Warn me....??"},
				{"Chronos":"After this, you will continue Gaia's tests, each harder than the last, with more of those creatures you just killed"},
				{"Elize":"How many are there?"},
				{"Chronos":"I do not know, for Gaia might add more test anytime"},
				{"Chronos":"However i do know this"},
				{"Chronos":"Eventually...... you will meet Gaia"},
				{"Elize":"What?? Where?? When??? Why????"},
				{"Chronos":"Yes."},
				{"Chronos":"Gaia likes to toy with her subjects"},
				{"Chronos":"And she would greet the exceptional ones..... personally..."},
				{"Chronos":"So if you survive enough of her tests..."},
				{"Elize":"She might consider meeting me personally...."},
				{"Chronos":"Exactly."},
				{"Elize":"But why tell me this?"},
				{"Elize":"You told me what Gaia wants, but what do YOU want?"},
				{"Elize":"Why did you give me your powers?"},
				{"Elize":"Why did you help me?"},
				{"Chronos":"You are right to doubt me."},
				{"Chronos":"While i do truly wish to help you with your trials..."},
				{"Chronos":"I do have my own reasons..."},
				{"Elize":"Which is.....?"},
				{"Chronos":"Information."},
				{"Elize":"Information?"},
				{"Chronos":"Yes."},
				{"Chronos":"As i said, eventually you will meet Gaia"},
				{"Chronos":"When you do, i need you to ask her about her plans."},
				{"Elize":"Her plans? about what?"},
				{"Chronos":"I suspect these tests she brought upon you is part of a grander scheme"},
				{"Chronos":"I could ask her myself, but she will not answer me."},
				{"Chronos":"That's where you come in."},
				{"Chronos":"Surely you want to know the whole reason she brought you here.."},
				{"Elize":"I do... and i am planning to ask that anyway...."},
				{"Elize":"But how would you get that information?"},
				{"Elize":"Can you really trust me with that? even if i don't lie to you i can still forget about some of it...."},
				{"Elize":"I'm..... not the best with respects to memory...."},
				{"Chronos":"Hahahaha.... i suspected as much."},
				{"Chronos":"Here, take this amulet."},
				{"Chronos":"It will activate when Gaia is near you, and when it happens, it will transmit any information to me."},
				{"Elize":"Well that's convenient."},
				{"Chronos":"I can stop time at will, and you think THAT is convenient."},
				{"Chronos":"In any case, i must once again return you to her world..."},
				{"Chronos":"Your next trial is about to begin.."},
				{"Elize":"*Sigh*"},
				{"Elize":"Allright then, let's do this."},
				{"Chronos":"Good luck, Elize"},
				{"Elize":"Wait... how did y.."}]
				
func _ready() -> void:
	chronos.visible = false
	elize.visible = true
	textfield.percent_visible = 0
	textfield.text = dialogue[count].values()[0]

func get_input():
	if Input.is_action_just_pressed("ui_accept"):
		if text_status == "finished":
			count += 1
			if count == dialogue.size():
				get_tree().change_scene(str("res://Scenes/Loading.tscn"))
			else:
				textfield.percent_visible = 0
				textfield.text = dialogue[count].values()[0]
				text_status = "running"
		elif text_status == "running":
			textfield.percent_visible = 1
			text_status = "finished"

func _process(delta: float) -> void:
	get_input()
	if count < dialogue.size():
		if dialogue[count].keys()[0] == "Chronos":
			chronos.visible = true
			elize.visible = false
		elif dialogue[count].keys()[0] == "Elize":
			elize.visible = true
			chronos.visible = false
		if textfield.percent_visible < 1:
			text_status = "running"
			textfield.percent_visible += delta
		else:
			text_status = "finished"


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass


func _on_skip_pressed() -> void:
	get_tree().change_scene(str("res://Scenes/Loading.tscn"))
