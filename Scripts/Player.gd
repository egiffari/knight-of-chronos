extends KinematicBody2D


var speed = 300.0
var jump_speed = 400.0
var GRAVITY = 1000.0

var velocity = Vector2()
var attacking = false
var jumping = false
var running = false
var falling = false
var double_jump = false
var dead = false
var failed = false

var track_position
# Called when the node enters the scene tree for the first time.

func _ready() -> void:
	$Animator.play("Idle")
	sprite_change($idle_sprite)
	$PlayerBGM.play()
			
func _physics_process(delta: float) -> void:
	check_BGM()
	check_fail()
	if !failed:
		check_dead()
	if !dead:
		get_input()
		velocity.y += delta * GRAVITY
		velocity = move_and_slide(velocity)

func check_BGM():
	if Global.ZA_WARUDO:
		if $PlayerBGM.volume_db > -80.0:
			$PlayerBGM.volume_db -= 1.0
		else:
			if track_position == null:
				track_position = $PlayerBGM.get_playback_position()
			$PlayerBGM.stop()
	elif !Global.ZA_WARUDO and Global.ZA_WARUDO_Activated:
		if !$PlayerBGM.playing:
			$PlayerBGM.play(track_position)
		if $PlayerBGM.volume_db < 0.0:
			$PlayerBGM.volume_db += 1
			
func check_dead():
	if dead:
		var knockback = Vector2()
		if $Death.flip_h == true:
			knockback.x = 100.0
		else:
			knockback.x = -100.0
		knockback = move_and_slide(knockback)
		if !$Animator.is_playing():
			failed = true
			sprite_change($Death)
			$Animator.play("death")
func check_fail():
	if failed and !$Animator.is_playing():
		get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
		
func check_idle():
	if !(jumping or attacking or running or falling or double_jump):
		sprite_change($idle_sprite)
		$Animator.play("Idle")
				
func check_attack():
	if Input.is_action_just_pressed("Attack"):
		sprite_change($attack_sprite)
		$Animator.play("Attack")
		if !$AttackingSFX.playing:
			$AttackingSFX.play(0)
		attacking = true
	if !$Animator.is_playing():
		attacking = false
		$AttackingSFX.stop()

func check_running():
	if Input.is_action_pressed("Right") or Input.is_action_pressed("Left"):
		running = true
		if !jumping and !falling:
			sprite_change($running_sprite)
			$Animator.play("Running")
		if Input.is_action_pressed("Right"):
			if $attack_sprite.flip_h == true:
				$attack_sprite/attack_hitbox.position.x += 15
				$attack_sprite/attack_hitbox/attack_collider.position.x += 15
			sprite_flip(false)
			velocity.x += speed
		if Input.is_action_pressed("Left"):
			if $attack_sprite.flip_h == false:
				$attack_sprite/attack_hitbox.position.x -= 15
				$attack_sprite/attack_hitbox/attack_collider.position.x -= 15
			sprite_flip(true)
			velocity.x -= speed
	elif Input.is_action_just_released("Right") or Input.is_action_just_released("Left"):
		running = false
		
func check_jumping():
	if !jumping and Input.is_action_just_pressed("Jump"):
		velocity.y -= jump_speed
		jumping = true
		sprite_change($jump_sprite)
		$Animator.play("Jump")
	if jumping and velocity.y == 0.0:
		jumping = false

func check_double_jump():
	if !double_jump and jumping and Input.is_action_just_pressed("Jump"):
		velocity.y = 0.0
		velocity.y -= jump_speed
		double_jump = true
		sprite_change($jump_sprite)
		$Animator.play("Jump")
	if double_jump and velocity.y == 0.0:
		double_jump = false
		
func check_falling():
	if velocity.y > 200:
		falling = true
		sprite_change($fall_sprite)
		$Animator.play("Fall")
	if velocity.y == 0:
		falling = false
		
func die():
	if !dead:
		dead = true
		sprite_change($Hit)
		$Animator.play("hit")

func get_input():
	velocity.x = 0
	check_idle()
	ZA_WARUDO()
	if !jumping and !falling:
		check_attack()
	check_double_jump()
	if !attacking:
		check_jumping()
		check_falling()
		check_running()
	
func sprite_change(exception: Node) -> void:
	for child in get_children():
		if child.get_class() == "Sprite" and child != exception:
			child.hide()
	exception.show()
	
func sprite_flip(direction: bool) -> void:
	for child in get_children():
		if child.get_class() == "Sprite":
			child.flip_h = direction
			
func _on_attack_hitbox_body_entered(body: Node) -> void:
	if "Enemy" in body.get_name():
		if body.position.x < self.position.x:
			body.sprite_flip(false)
		if body.position.x > self.position.x:
			body.sprite_flip(true)
		body.check_hit()

func ZA_WARUDO():
	if Input.is_action_just_pressed("ZA_WARUDO") and !Global.ZA_WARUDO_Activated:
		Global.ZA_WARUDO = true
		Global.ZA_WARUDO_Activated = true


