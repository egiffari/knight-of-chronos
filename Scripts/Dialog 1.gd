extends Node2D

onready var textfield = $Dialog_box/Control/Outerbox/Innerbox/textfield
onready var chronos = $Characters/Chronos
onready var elize = $Characters/Elize
var text_status
onready var count = 0
var dialogue = [{"Elize":"Huh. . .?"}, 
				{"Elize":"Where am i?"}, 
				{"Chronos":"I see you have finally awakened,"},
				{"Elize":"Huh? Who are you? what is happening..."},
				{"Elize":"!"},
				{"Elize":"What... are those...?"},
				{"Elize":"They don't seem to be moving... but..."},
				{"Chronos":"Ease yourself, they can't move in here."},
				{"Elize":"Yeah, i thin.. wait who are you?"},
				{"Chronos":"*Sigh*"},
				{"Chronos":"My name is Chronos."},
				{"Chronos":"I am the master of time, and this is my domain"},
				{"Elize":"Your domain? so this place is yours?"},
				{"Chronos":"No, this 'place' is not mine, i merely own it's 'time'"},
				{"Chronos":"For you see, we are in the gap between time"},
				{"Chronos":"In here, time stands still, and only moves at my command"},
				{"Chronos":"but the space within this world have it's own master"},
				{"Chronos":"The one who brought you here, the Mistress of Space"},
				{"Chronos":"Gaia. . ."},
				{"Elize":"Gaia? Who is that? Where is she? Why did she brought me here?"},
				{"Chronos":"I am sure you are riddled with questions, and i will answer them."},
				{"Chronos":"But not now, as i cannot keep you here much longer"},
				{"Elize":"But i thought you were the master of time?"},
				{"Chronos":"I am, but my powers do not affect Gaia"},
				{"Chronos":"Sooner or later, she will realize what is happening"},
				{"Chronos":"I will return you to her domain, where time flows, and the creatures remain"},
				{"Elize":"So you're just going to throw me back with them??"},
				{"Chronos":"Do not be afraid, i am not letting you return as is."},
				{"Chronos":"I am giving you an armor, made with my own flesh."},
				{"Chronos":"In order for you to be able to move, i had to make it as light as i can"},
				{"Chronos":"It won't protect you from a well-placed strike...."},
				{"Chronos":"But it will protect you from other sources"},
				{"Chronos":"For example..... yes.... let's say you would fall from high ground"},
				{"Chronos":"It will absorb the impact so you can land safely."},
				{"Chronos":"Well.... that's assuming you actually land...."},
				{"Elize":"Wh...what do you mean....?"},
				{"Chronos":"See that there? that is the abyssal."},
				{"Elize":"The abyssal?"},
				{"Chronos":"Yes. it is an area where mortals could not enter."},
				{"Chronos":"It's sheer forces will tear your body apart from within... an armor, no matter how strong, would not help you."},
				{"Elize":"*gulp*"},
				{"Elize":"well.... better watch my steps then....."},
				{"Chronos":"You'd be wise to do just that."},
				{"Chronos":"And also...."},
				{"Chronos":"Take this sword..."},
				{"Chronos":"It is forged from the hands of the World Clock"},
				{"Elize":"But why give me this? am i supposed to fight them?"},
				{"Chronos":"Yes."},
				{"Chronos":"Those creatures are vile"},
				{"Chronos":"They will kill anything that moves and are unlike them"},
				{"Chronos":"You have to kill them, or they will kill you"},
				{"Elize":"But i can't fight! how am i supposed to kill them?"},
				{"Chronos":"I have given you part of my power"},
				{"Chronos":"You now possess a small part of my strength, and my knowledge of combat"},
				{"Chronos":"You can fight now, you just don't know it yet"},
				{"Elize":"but... is it enough?"},
				{"Chronos":"I only gave you a small part of my power, but they should suffice"},
				{"Elize":"I might have that and your knowledge, but i'm still not you"},
				{"Elize":"I...I don't have any experience fighting at all! what if i screw up??"},
				{"Chronos":"hmm..... what i gave you should be enough, but still, in the case of an emergency...."},
				{"Chronos":"I have also given you my power to stop time"},
				{"Elize":"Wait, WHAT?"},
				{"Elize":"Well then this would be easy! i can just stop time and kill them all!"},
				{"Chronos":"Not necessarily.."},
				{"Chronos":"I might have given you part of my power but your body is still that of a mortal"},
				{"Chronos":"You can stop time at your will, but only once, and only for a little while"},
				{"Chronos":"Anymore than that, and your body will crumble"},
				{"Elize":"So what do i do?"},
				{"Chronos":"Survive..."},
				{"Chronos":"After all, you might have been strengthened, but that won't stop a blade from slicing you in half"},
				{"Elize":"Well that's reassuring...."},
				{"Chronos":"Fear not, use your newfound powers cautiously, and you can end them with ease"},
				{"Chronos":"Once you've killed them all, Gaia will move you to a different world."},
				{"Chronos":"When she does, i can bring you here to the gap in time once again, i will answer your questions then"},
				{"Chronos":"For now, remember what i told you, and live."},
				{"Elize":"Wait... but... i still...."},
				{"Chronos":"I cannot keep you here any longer, you must return"},
				{"Elize":"Guess i have no choice then"},
				{"Chronos":"I'm afraid not, good luck mortal, and may we meet again"},]


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	chronos.visible = false
	elize.visible = true
	textfield.percent_visible = 0
	textfield.text = dialogue[count].values()[0]

func get_input():
	if Input.is_action_just_pressed("ui_accept"):
		if text_status == "finished":
			count += 1
			if count == dialogue.size():
				get_tree().change_scene(str("res://Scenes/Loading.tscn"))
			else:
				textfield.percent_visible = 0
				textfield.text = dialogue[count].values()[0]
				text_status = "running"
		elif text_status == "running":
			textfield.percent_visible = 1
			text_status = "finished"

func _process(delta: float) -> void:
	get_input()
	if count < dialogue.size():
		if dialogue[count].keys()[0] == "Chronos":
			chronos.visible = true
			elize.visible = false
		elif dialogue[count].keys()[0] == "Elize":
			elize.visible = true
			chronos.visible = false
		if textfield.percent_visible < 1:
			text_status = "running"
			textfield.percent_visible += delta
		else:
			text_status = "finished"


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass


func _on_skip_pressed() -> void:
	get_tree().change_scene(str("res://Scenes/Loading.tscn"))
