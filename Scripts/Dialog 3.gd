extends Node2D


onready var chronos = $Characters/Chronos
onready var elize = $Characters/Elize
onready var textfield = $Dialog_box/Control/Outerbox/Innerbox/textfield

var count = 0
var text_status

var dialogue = [{"Elize":"Allright... that's another one done..."},
				{"Chronos":"You've done well."},
				{"Elize":"HOLY &*%^!"},
				{"Elize":"Don't scare me like that!"},
				{"Chronos":"What are you talking about?"},
				{"Elize":"What do you mean what i'm talking ab...."},
				{"Elize":"*Sigh*"},
				{"Elize":"So why did you call me this time?"},
				{"Chronos":"You have a lot more trials ahead of you..."},
				{"Chronos":"I am offering you momentary rest."},
				{"Elize":"Oh.... thanks"},
				{"Elize":"I've only done two and it's already tiring..."},
				{"Elize":"I can't imagine what the next trials are like..."},
				{"Elize":"well uhh... i guess i'll take your word for it.."},
				{"Chronos":"Good... sleep young mortal, for the trials ahead are dark, and unforgiving"}]

func _ready() -> void:
	chronos.visible = false
	elize.visible = true
	textfield.percent_visible = 0
	textfield.text = dialogue[count].values()[0]

func get_input():
	if Input.is_action_just_pressed("ui_accept"):
		if text_status == "finished":
			count += 1
			if count == dialogue.size():
				get_tree().change_scene(str("res://Scenes/Loading.tscn"))
			else:
				textfield.percent_visible = 0
				textfield.text = dialogue[count].values()[0]
				text_status = "running"
		elif text_status == "running":
			textfield.percent_visible = 1
			text_status = "finished"

func _process(delta: float) -> void:
	get_input()
	if count < dialogue.size():
		if dialogue[count].keys()[0] == "Chronos":
			chronos.visible = true
			elize.visible = false
		elif dialogue[count].keys()[0] == "Elize":
			elize.visible = true
			chronos.visible = false
		if textfield.percent_visible < 1:
			text_status = "running"
			textfield.percent_visible += delta
		else:
			text_status = "finished"


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass


func _on_skip_pressed() -> void:
	get_tree().change_scene(str("res://Scenes/Loading.tscn"))
