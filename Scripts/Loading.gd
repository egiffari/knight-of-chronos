extends Node2D


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if Global.level == 3:
		get_tree().change_scene(str("res://Scenes/WinScreen.tscn"))

func _process(delta: float) -> void: 
	$Label.text = str(ceil($Timer.time_left))
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass


func _on_Timer_timeout() -> void:
	get_tree().change_scene(str("res://Scenes/Level " + str(Global.level) + ".tscn"))
