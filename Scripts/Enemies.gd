extends Node2D

var enemy_count = 1
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
var player
var rel_dist_x
var rel_dist_y
var relative_distance
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	player = self.get_parent().get_child(0)
	
func _process(delta: float) -> void:
	Global.playtime += delta
	var child_count = 0
	for child in get_children():
		child_count += 1
		if !Global.ZA_WARUDO:
			rel_dist_x = player.position.x - child.position.x
			rel_dist_y = player.position.y - child.position.y
			relative_distance = sqrt((rel_dist_x*rel_dist_x) + (rel_dist_y*rel_dist_y))
			if relative_distance < 40:
				if child.get_child(1).flip_h == player.get_child(1).flip_h:
					for grandchild in child.get_children():
						if grandchild.get_class() == "Sprite":
							grandchild.flip_h = !grandchild.flip_h
				child.attack()
	enemy_count = child_count
	if enemy_count == 0:
		Global.level += 1
		get_tree().change_scene(str("res://Scenes/Dialog " + str(Global.level) +".tscn"))

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
