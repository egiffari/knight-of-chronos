extends Node2D


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var clear_time = Global.playtime
	var minutes = 0
	var seconds = 0.0
	while clear_time > 60:
		minutes += 1
		clear_time -= 60
	seconds = stepify(clear_time, 0.01)
	if minutes > 0:
		$playtime.text = "clear time: " + str(minutes) +" m " + str(seconds) + " s"
	else:
		$playtime.text = "clear time: " + str(seconds) + " s"

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass


func _on_LinkButton_pressed() -> void:
	get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))
