extends Node2D


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Global.ZA_WARUDO = false
	Global.ZA_WARUDO_Activated = false
	Global.level = 0

func _process(delta: float) -> void:
	if $menu_music.volume_db < 0.0:
		$menu_music.volume_db += 1


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass


func _on_start_pressed() -> void:
	Global.level += 1
	get_tree().change_scene(str("res://Scenes/Dialog " + str(Global.level) + ".tscn"))
