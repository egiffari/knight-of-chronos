extends KinematicBody2D

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

export var left_border = 0.0
export var right_border = 0.0
var direction = 1

var dead = false
var destroyed = false
var attacking = false
var patrolling = true
var idle = false

var knockback = Vector2()
var velocity = Vector2()
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$Animator.play("Idle")
	sprite_change($Idle)
		
func patrol():
	if !dead:
		patrolling = true
		attacking = false
		velocity.x = direction * 100.0
		if get_position().x < left_border:
			direction = 1
		elif get_position().x > right_border:
			direction = -1
		sprite_change($Run)
		$Animator.play("Run")
		if get_position().x < left_border:
			sprite_flip(false)
			check_idle()
		elif get_position().x > right_border:
			sprite_flip(true)
			check_idle()
		
	
func check_hit():
	dead = true
	attacking = false
	sprite_change($Death)
	$Animator.play("death")
	$DyingSFX.play(0.50)
	self.get_child(0).queue_free()

func check_death():
	if dead == true:
		if $Death.flip_h == true:
			knockback.x = 100.0
		else:
			knockback.x = -100.0
		velocity = Vector2(0.0, 0.0)
		knockback = move_and_slide(knockback)
		if !$Animator.is_playing():
			destroyed = true
			queue_free()

func check_idle():
	velocity.x = 0
	idle = true
	sprite_change($Idle)
	$Animator.play("Idle")
	$patrolDelay.start()

func attack():
	if !dead:
		velocity.x = 0
		attacking = true
		$Animator.play("attack")
		if !$AttackingSFX.playing:
			$AttackingSFX.play(0)
		sprite_change($Attack)

func _physics_process(delta: float) -> void:
	if !destroyed:
		check_death()
		if attacking and !$Animator.is_playing():
			attacking = false
			patrol()
		if !Global.ZA_WARUDO:
			if !attacking and !idle:
				patrol()
				velocity.y = delta * 1000.0
				velocity = move_and_slide(velocity)
			
		else:
			$Animator.stop()
	
func sprite_change(exception: Node) -> void:
	for child in get_children():
		if child.get_class() == "Sprite" and child != exception:
			child.hide()
	exception.show()
	
func sprite_flip(direction: bool) -> void:
	for child in get_children():
		if child.get_class() == "Sprite":
			child.flip_h = direction

func _on_AttackCollider_Body_Entered(body: Node) -> void:
	if body.get_name() == "Player":
		if body.position.x < self.position.x:
			body.sprite_flip(false)
		if body.position.x > self.position.x:
			body.sprite_flip(true)
		body.die()


func _on_patrolDelay_timeout() -> void:
	idle = false
	if $Idle.flip_h == true:
		position.x -= 1
	else:
		position.x += 1
